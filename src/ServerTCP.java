import java.io.*;
import java.net.*;

class ServerTCP  {

    /* A COMPLETER :
       - déclaration des attributs
     */
	ObjectInputStream ois = null;
	ObjectOutputStream oos = null;
	ServerSocket conn =null;
	Socket commReq = null;
	Game game = null;
	ServerThread serverThread;

    public ServerTCP(int serverPort) throws IOException {

	/* A COMPLETER :
	   - instanciation ServerSocket et Game
	 */ //DONE
		try {
			conn = new ServerSocket(serverPort);
		}
		catch(IOException e) {
			System.out.println("problème creation socket serveur : "+e.getMessage());
			System.exit(1);
		}
		game = new Game();
    }

    public void mainLoop() throws IOException,ClassNotFoundException {

	while (true) {

	    /* A COMPLETER :
	       - accepter connexion
	       - créer un ServerThread (cf. la classe pour le constructeur) et le lancer
	     */ //DONE
		try {
			commReq = conn.accept();
			ois = new ObjectInputStream(commReq.getInputStream());
			oos = new ObjectOutputStream(commReq.getOutputStream());
			serverThread = new ServerThread(0,commReq,game);
			serverThread.run();
		}
		catch(IOException e) {
			System.out.println("probleme communication : "+e.getMessage());
		}
	}
    }
}
